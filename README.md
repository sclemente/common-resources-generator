# COMMON-RESOURCES-GENERATOR

Este es un repositorio hecho para su distribución en `bower`. Contiene los archivos necesarios para la generación de la carpeta `common/` del repositorio de EYPH, utilizando para ello el paquete `common-resources`.

## Instalación

```shell
git clone http://USUARIO@lcm-repositorio-fuentes.igrupobbva/scm/eyph/common-resources-generator.git
```

## Uso

La utilidad de este paquete está restringida a la generación del distribuido de todos los archivos comunes a todas las aplicaciones de EYPH.

El contenido del `bower.json` se muestra a continuación, detallando así las versiones que se utilizan de cada una de las librerías.

```javascript
{
  "name": "common-resources-generator",
  "version": "0.0.0",
  "dependencies": {
    "angular": "^1.4.0",
    "bootstrap-sass-official": "^3.2.0",
    "angular-animate": "^1.4.0",
    "angular-aria": "^1.4.0",
    "angular-cookies": "^1.4.0",
    "angular-messages": "^1.4.0",
    "angular-resource": "^1.4.0",
    "angular-route": "^1.4.0",
    "angular-sanitize": "^1.4.0",
    "angular-touch": "^1.4.0",
    "angular-ui-router": "^0.2.18",
    "angular-bootstrap": "^1.1.2",
    "angular-translate": "~2.9.1",
    "angular-translate-loader-url": "~2.9.1",
    "angular-translate-loader-static-files": "~2.9.1",
    "jquery": "~2.2.0",
    "jquery-ui": "~1.11.4",
    "highcharts": "~4.2.3",
    "angular-ui": "~0.4.0",
    "angular-translate-storage-local": "~2.9.2",
    "angular-translate-loader-partial": "~2.9.2",
    "ngMask": "angular-mask#~3.0.16",
    "ng-file-upload": "~12.0.1",
    "ng-iban": "~1.1.0",
    "common-resources": "http://USUARIO@lcm-repositorio-fuentes.igrupobbva/scm/eyph/common-resources.git "
  },
  "devDependencies": {},
  "appPath": "app",
  "overrides": {
    "bootstrap-sass-official": {
      "main": [
        "less/bootstrap.less",
        "dist/css/bootstrap.css",
        "dist/js/bootstrap.js"
      ]
    },
    "highcharts": {
      "main": [
        "highcharts.js",
        "highcharts-more.js",
        "modules/exporting.js",
        "modules/drilldown.js"
      ]
    }
  }
}
```

Al ser un proyecto generador su funcionamiento es ligeramente distinto al resto, por lo que el grunt contiene tareas diferentes a las del resto de proyectos "convencionales"

### grunt build

Esta tarea se utilizará en el caso de que sea necesario incluir alguna librería externa adicional en el `vendor.js`. En principio, no debería ser necesario ejecutarla más allá de la subida inicial.

### grunt onlyStatic

Esta tarea generará los estilos y las imágenes que sí que serán modificadas a lo largo del proceso de creación de los proyectos.

### grunt onlyJs

Esta tarea generará únicamente el `vendor.js` con todas las librerías de la aplicación, que será el que se incluya en el paquete `common-resources`.

## A tener en cuenta

* Para la correcta generación del distribuido se han eliminado del `index.html` las etiquetas `<!-- bower:js -->`, para evitar errores derivados de la importación del `common-resources`. Por ello, si en algún momento es necesaria incluir alguna otra librería externa, habrá que añadirla a mano al html.
* Todo el contenido de esta carpeta irá repositado en la carpeta `common` del repositorio de EYPH.
